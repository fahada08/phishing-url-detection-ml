from flask import Flask
from flask import render_template
from flask import request
import numpy as np
import pickle
from features_extractor import FeatureExtraction
import warnings 

warnings.filterwarnings('ignore')

app = Flask(__name__)

gbc = pickle.load(open('pickle/imp_gbc.pkl','rb'))

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/index.html')
def home():
    return render_template('index.html')

@app.route('/predict', methods=["GET", "POST"])
def predict():
    if request.method == "POST":
        url = request.form.get("url")

        obj = FeatureExtraction(url)
        
        x = np.array(obj.getFeaturesList()).reshape(1,30) 

        # Creating a dictionary to store the feature names and their detection status
        features_list = ['IP Address', 'Long URL', 'Short URL', 'Symbol@', 'Redirection //',
       'Prefix Suffix-', 'Sub Domains', 'HTTPs', ' Domain Registration Lenght ', 'Favicon',
       'Non-Standard Port', 'HTTPS Domain in URL', 'Request URL', 'Anchor URL',
       'Links in Script Tags', 'Server Form Handler', 'Info-Email', 'Abnormal URL',
       'Website Forwarding', 'Status Bar Customization', 'Disable Right Click',
       'Using Popup Window', 'Iframe Redirection', 'Age of Domain', 'DNS Record',
       'Website Traffic', 'Page Rank', 'Google Index', 'Links Pointing To Page',
       'Statistical Report']

        features_description_legit = {
                    'IP Address': 'IP Address not detected in URL',
                    'Long URL': 'Length URL is appropriate and inline with preferred threshold',
                    'Short URL': 'Length of URL is less than / equal to 25 characters',
                    'Symbol@': '"@" symbol is not detected ',
                    'Redirection //': 'Presence of "//" after the protocol (http, https).',
                    'Prefix Suffix-': 'PrefixSuffix"-" symbol detected in domain name.',
                    'Sub-Domains': ' Subdomains detected in URL.',
                    'HTTPS': 'HTTPS Protocol Detected.',
                    'Domain Registration Length': 'Number of months the domain has been registered.',
                    'Favicon': 'Presence of favicon in the webpage.',
                    'Non-Standard Port': 'Use of non-standard port number in the URL.',
                    'HTTPS Domain in URL': 'Presence of domain name in the URL.',
                    'Request URL': 'Presence of request URL in the webpage.',
                    'Anchor URL': 'Presence of anchor URL in the webpage.',
                    'Links in Script Tags': 'Presence of links in the script tags.',
                    'Server Form Handler': 'Presence of server form handler in the webpage.',
                    'Info Email': 'Presence of email address instead of domain name in the URL.',
                    'Abnormal URL': 'Presence of any abnormal character or symbol in the URL.',
                    'Website Forwarding': 'Use of website forwarding technique in the URL.',
                    'Status Bar Customization': 'Customization of status bar in the webpage.',
                    'Disable Right Click': 'Disabling of right click option in the webpage.',
                    'Using Popup Window': 'Use of pop-up windows in the webpage.',
                    'Iframe Redirection': 'Use of Iframe redirection technique in the webpage.',
                    'Age of Domain': 'Age of the domain in number of years.',
                    'DNS Record': 'Presence of DNS record for the domain.',
                    'Website Traffic': 'Number of visitors visiting the webpage.',
                    'Page Rank': 'Page rank of the webpage.',
                    'Google Index': 'Index status of the webpage in Google search engine.',
                    'Links Pointing to Page': 'Number of external links pointing to the webpage.',
                    'Statistical Report': 'Use of statistical report in the webpage.'
        }

        features_description_phish = {
                    'IP Address': 'Presence of IP address instead of domain name in the URL.',
                    'Long URL': 'URLs with length greater than or equal to 75 characters.',
                    'Short URL': 'URLs with length less than or equal to 25 characters.',
                    'Symbol@': 'Presence of "@" symbol in the URL.',
                    'Redirection //': 'Presence of "//" after the protocol (http, https).',
                    'Prefix Suffix-': 'Presence of "-" symbol in the domain name.',
                    'Sub Domains': 'Presence of subdomains in the URL.',
                    'HTTPs': 'Use of HTTPS protocol instead of HTTP.',
                    'Domain Registration Length': 'Number of months the domain has been registered.',
                    'Favicon': 'Presence of favicon in the webpage.',
                    'Non-Standard Port': 'Use of non-standard port number in the URL.',
                    'HTTPS Domain in URL': 'Presence of domain name in the URL.',
                    'Request URL': 'Presence of request URL in the webpage.',
                    'Anchor URL': 'Presence of anchor URL in the webpage.',
                    'Links in Script Tags': 'Presence of links in the script tags.',
                    'Server Form Handler': 'Presence of server form handler in the webpage.',
                    'Info Email': 'Presence of email address instead of domain name in the URL.',
                    'Abnormal URL': 'Presence of any abnormal character or symbol in the URL.',
                    'Website Forwarding': 'Use of website forwarding technique in the URL.',
                    'Status Bar Customization': 'Customization of status bar in the webpage.',
                    'Disable Right Click': 'Disabling of right click option in the webpage.',
                    'Using Popup Window': 'Use of pop-up windows in the webpage.',
                    'Iframe Redirection': 'Use of Iframe redirection technique in the webpage.',
                    'Age of Domain': 'Age of the domain in number of years.',
                    'DNS Record': 'Presence of DNS record for the domain.',
                    'Website Traffic': 'Number of visitors visiting the webpage.',
                    'Page Rank': 'Page rank of the webpage.',
                    'Google Index': 'Index status of the webpage in Google search engine.',
                    'Links Pointing to Page': 'Number of external links pointing to the webpage.',
                    'Statistical Report': 'Use of statistical report in the webpage.'
        }
      
        features_detected = {}
        features_desp = {}

        for i in range(len(features_list)):
            if x[0][i] == -1:
                features_detected[features_list[i]] = " Found " + " -> " + features_description_phish.get(features_list[i], "Data not available")
            elif x[0][i] == 1:
                features_detected[features_list[i]] = " Not Found " +" -> "+ features_description_legit.get(features_list[i], "Data not available")
            elif x[0][i] == 0:
                features_detected[features_list[i]] = " Suspicious " +" -> "+ features_description_phish.get(features_list[i], "Data not available")
  
        ph = gbc.predict_proba(x)[0,0]
        lg = gbc.predict_proba(x)[0,1]  
        
        try:
            if gbc.predict(x) == -1:
                return render_template('index.html', label = -1, url = url, x = x[0], ph = round((ph*100),2),
                                   features_detected=features_detected, features_desp=features_description_phish)
            elif gbc.predict(x) == 1:
                return render_template('index.html', label = 1, url = url, x = x[0], lg = round((lg*100),2), 
                                   features_detected=features_detected, features_desp=features_description_legit)
            else:
                return render_template('index.html', label = 0, url = url, x = x[0], lg = round((lg*100),2), 
                                   features_detected=features_detected, features_desp=features_description_phish)
        except:
            return render_template('index.html', label = -1, url = url, x = x[0], ph = round((ph*100),2),
                                   features_detected=features_detected, features_desp=features_description_phish)

if __name__ == '__main__':
    app.run(debug=False)